#!/bin/bash
# This ping every possible ip address in the subet (based on the subnet mask).
# If the communication is alive the it prints the IP.

PAT='^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){2}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$'
# Check if there is argument
if [ "$1" == "" ]; then
	echo "You provided no IP"
	exit 22
elif [[ ! "$1" =~ $PAT ]]; then
	echo "Proivded IP is not valid"
	exit 22
else
	for i in {1..254}; do
		ping -c 1 -W 0.5 -s 1 $1.$i | grep "9 bytes" | cut -d " " -f 4 | tr -d ":" &
	done
fi
wait
